/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/WLogger>

#include <algorithm>
#include <string>

#include "dbo_orm_base.hpp"

#ifndef DBO_ORM_SQLITE_H
#define DBO_ORM_SQLITE_H

namespace dbo = Wt::Dbo;

namespace qfscanner{

class ORMSqlLite: public ORMBase{
    

    dbo::backend::Sqlite3 *sqlite3;
    void init_dbconnection();
public:

    ORMSqlLite(const std::string & dbname);
    
    ~ORMSqlLite(){
        delete this->sqlite3;
    }


};



}
#endif