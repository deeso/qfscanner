/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
#include <Wt/Dbo/Dbo>
#include <string>

#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/backend/Postgres>


#include <boost/filesystem.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>


#include <openssl/sha.h>
#include <openssl/md5.h>

#include <magic.h>
#include <cstring>

#include <algorithm>
#include <string>

#include <PeLib.h>
#include <buffer/InputBuffer.h>

#include <boost/container/set.hpp>
#include <exception>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "dbo_orm_defs.hpp"


#include <stdlib.h>     
#include <time.h>

#ifndef DBO_ORM_BASE_H
#define DBO_ORM_BASE_H

namespace dbo = Wt::Dbo;

namespace qfscanner{

enum HASH_CACHE_TYPE { IMPORTS, EXPORTS, PEFILES, IMPORTS_ASSOC, EXPORTS_ASSOC };

extern boost::container::set<long> IMPORT_HASHES;
extern boost::container::set<long> EXPORT_HASHES;
extern boost::container::set<long> PEFILES_HASHES;
extern boost::container::set<long> EXPORTS_ASSOC_HASHES;
extern boost::container::set<long> IMPORTS_ASSOC_HASHES;

extern volatile bool SRAND_CALLED;
 
class ORMException : public std::exception
{
    std::string s;
public:
    ORMException(std::string ss) : s(ss) {}
    const char* what() const throw() 
    { 
        return s.c_str(); 
    }
    ~ORMException () throw() {};// throw {}
};


class ORMBase: public PeLib::PeFileVisitor{
    
    std::string badpefile_log;


    void init_hash_cache(  HASH_CACHE_TYPE x);
    bool check_hash_cache(  HASH_CACHE_TYPE x, long hash);
    void update_hash_cache(  HASH_CACHE_TYPE x, long hash);
    
    uint64_t processed;


    template<int bits>
    void process_sections(PeLib::PeFileT<bits>& pef,
                          long hash_id){
        std::stringstream ss_log (std::stringstream::in | std::stringstream::out);

        dbo::Transaction parse_sections(this->session);

        dbo::ptr<PEFile> dbo_pefile = this->session.find<PEFile>().where("hash_id = ?").bind(hash_id);
        parse_sections.commit();
        this->session.flush();

        PeLib::PeHeaderT<bits>& peh = (pef).peHeader();
        std::string filename = pef.getFileName();
        //dbo::Transaction transaction(session);

        for (int i=0;i<peh.getNumberOfSections();i++)
        {
            
            std::string sectionName = peh.getSectionName(i);
            ss_log << "[=] Processing Section: " << sectionName;
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            uint64_t vAddr = peh.getVirtualAddress(i);
            PeLib::dword vSize = peh.getVirtualSize(i),
                 rSize = peh.getSizeOfRawData(i),
                 ptrData = peh.getPointerToRawData(i),
                 characteristics = peh.getCharacteristics();

            uint64_t dataSz =  0;

            ss_log << "[=] Calulating Section Entropy.";
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            //XXX Disabled the Entropy calculation to speed up analysis
            float *entropy;// = new float;
	    *entropy = 0.0;
            if(rSize == 0)
            {
                // XXX Disabled here
                entropy = calcEntropy(filename, ptrData, vSize);
                if (entropy == NULL)
                {
                    ss_log << "[-] Failed to calulate Section Entropy.";
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                }                 
                
            }
            else
            {
                // XXX Disabled here
                entropy = calcEntropy(filename, ptrData, rSize);
                if (entropy == NULL)
                {
                    ss_log << "[-] Failed to calulate Section Entropy.";
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                }                 

            }

            if (entropy == NULL)
            {   
                entropy = new float;
                *entropy = 0.0;
            }                 



            ss_log << "Calulated Section Entropy: " << *entropy;
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            Section *section = new Section(dbo_pefile->FileName,
                                            sectionName, vSize, rSize, 
                                            vAddr, *entropy, characteristics);
            
            ss_log << "Section: " << (section->SectionName) << " for PE: ";
            ss_log << (dbo_pefile->FileName) << " has hash_id: " << (section->hash_id);
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            dbo::ptr<Section> dbo_section = session.add(section);
            dbo_section.modify()->pefile = dbo_pefile;

            if (dbo_pefile->EntryPoint <= vAddr &&
                 dbo_pefile->EntryPoint <= (vAddr + vSize))
            {
                dbo_pefile.modify()->SectionNameOEP =  section->SectionName;
                //dbo_section.modify()->SectionOEP =  dbo_pefile;
            }
  
            try
            {

                ss_log << "[+] Completed Processing Section: " << section->SectionName;
                
                LOG4CXX_DEBUG(this->myLogger, ss_log.str());
                ss_log.str("");            
                
            }
            catch(std::exception & e)
            {
                    
                ss_log << "[-] Failed to add Section: " << section->SectionName << std::endl;
                ss_log << e.what();             
                LOG4CXX_WARN(this->myLogger, ss_log.str());
                ss_log.str("");

            }

            delete entropy;

        }
        //transaction.commit();

        ss_log << "[+] Completed Sections processing for: " << filename;
        //ss_log << "Adding sections to: " << dbo_pefile->hash_id ;

        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");
            
    }



    template<int bits>
    void process_imports(PeLib::PeFileT<bits>& pef, 
                         dbo::ptr<PEFile> dbo_pefile,
                         PEFile *pefilePtr){


        dbo::Transaction process_imports_tran(this->session);

        std::vector< dbo::ptr<Import> > dbo_import_vector;

        std::stringstream ss_log (std::stringstream::in | std::stringstream::out);
        boost::container::set<long> dbos_to_query;

        const PeLib::ImportDirectory<bits>& imp = static_cast<PeLib::PeFileT<bits>&>(pef).impDir();

        int numberOfImports = imp.getNumberOfFiles(PeLib::OLDDIR);
        
        if (numberOfImports == 0)
            return;
        
        ss_log << "process_imports: Current PEFile.hash_id: " << dbo_pefile->hash_id;    
        LOG4CXX_TRACE(this->myLogger, ss_log.str());
        ss_log.str("");
        //dbo::Transaction transaction_add_import_associations(this->session);

            for (unsigned int i=0;i<numberOfImports;i++)
            {        
                
                std::string DllName = imp.getFileName(i, PeLib::OLDDIR);                    
                for (unsigned int j=0;j<imp.getNumberOfFunctions(i, PeLib::OLDDIR);j++)
                {
                    std::string functionName = imp.getFunctionName(i, j, PeLib::OLDDIR);

                    if (functionName == std::string("")){
                        std::stringstream hint;
                        hint << std::hex << imp.getFunctionHint(i, j, PeLib::OLDDIR);
                        functionName = hint.str();
                    }

                    Import *importPtr = new Import(functionName, DllName);

                    if (importPtr == NULL){
                        ss_log << "importPtr is NULL, skipping";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                        continue;                        
                    }

                    if (this->check_hash_cache(IMPORTS, importPtr->hash_id)){
                        ss_log << "Import("<<importPtr->hash_id<<") does exist: " << DllName << ":" << functionName;    
                        LOG4CXX_TRACE(this->myLogger, ss_log.str());
                        ss_log.str("");
                        dbos_to_query.insert(importPtr->hash_id);
                        delete importPtr;

                    }else{
                        ss_log << "Import("<<importPtr->hash_id<<") does not exist: " << DllName << ":" << functionName;    
                        LOG4CXX_TRACE(this->myLogger, ss_log.str());
                        ss_log.str("");

                        dbo::ptr<Import> dbo_import = session.add(importPtr);
                        this->update_hash_cache(IMPORTS, importPtr->hash_id);
                        dbo_import_vector.push_back(dbo_import);
                    }
                    
                }        
            }
            ss_log << "[=] Querying DB and creating ImportAssociations if they don't exist.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");
            
            
            for(boost::container::set<long>::iterator it = dbos_to_query.begin(); 
                it != dbos_to_query.end(); it++){
                
                ss_log << "Querying hash_id = " << *it;    
                LOG4CXX_TRACE(this->myLogger, ss_log.str());
                ss_log.str("");

                dbo::ptr<Import> dbo_import = this->session.find<Import>().where("hash_id = ?").bind(*it);
                if (dbo_import){
                    // do nothing
                }else
                {
                    ss_log << "[-] dbo_import ("<< *it << ") was NULL, so it is probably not in the DB.";    
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");
                    continue;                    
                }


                ImportAssociations *importAssocPtr = new ImportAssociations(*dbo_import, *pefilePtr);
                if (importAssocPtr != NULL && !check_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id)){
                    ss_log << "dbo_import ("<<  *it << ") and dbo_pefile (" << dbo_pefile->hash_id << ")";    
                    //ss_log << "insert operation.\n";
                    LOG4CXX_TRACE(this->myLogger, ss_log.str());
                    ss_log.str("");
              
                    
                    dbo::ptr<ImportAssociations> dbo_import_assoc = session.add(importAssocPtr);
                    dbo_import_assoc.modify()->import_relation = dbo_import;
                    dbo_import_assoc.modify()->pefile = dbo_pefile; 
                    update_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id);
                }else{
                    ss_log << "[-] import_association ("<< *it << ") was already in the DB.";    
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");
                    continue;                    

                    if (importAssocPtr)
                    {
                        delete importAssocPtr;
                    }
                    else
                    {
                        ss_log << "[-] importAssocPtr was NULL, something bad is happening.";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                    }
                }
            }
            ss_log << "[+] Done creating Import Associations for existing imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");

            ss_log << "[=] Creating Import Associations for new imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");

            std::vector< dbo::ptr<Import> >::iterator it_imports = dbo_import_vector.begin();
            for (; it_imports != dbo_import_vector.end(); it_imports++){
                ImportAssociations *importAssocPtr = new ImportAssociations(*(*it_imports), *pefilePtr);
                if (importAssocPtr != NULL && !check_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id)){
                    //ss_log << "dbo_import ("<< (*(*it_imports)).hash_id << ") and dbo_pefile (" << dbo_pefile->hash_id << ")\n";    
                    //ss_log << "insert operation.\n";
                    //LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                    dbo::ptr<ImportAssociations> dbo_import_assoc = session.add(importAssocPtr);
                    dbo_import_assoc.modify()->import_relation = *it_imports;
                    dbo_import_assoc.modify()->pefile = dbo_pefile; 
                    update_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id);
                }else{
                    if (importAssocPtr)
                        delete importAssocPtr;
                    else
                    {
                        ss_log << "[-] importAssocPtr was NULL, something bad is happening.";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                    }
                }   
            }
            ss_log << "[+] Done creating Import Associations for new imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");


            ss_log << "Committing imports and associations.";    
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            //transaction_add_import_associations.commit();
        ss_log << "[+] Completed processing the imports.";    
        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");
        process_imports_tran.commit();
        this->session.flush();


    }

    void create_file_summary(PEFile *pefilePtr);
    void process_pefile(PeLib::PeFile32 &file);
    void process_pefile(PeLib::PeFile64 &file);
    
    void process_exports(dbo::ptr<PEFile> dbo_pefile,
                         PEFile *pefilePtr, 
                    PeLib::ExportDirectory& exp);


    virtual void init_dbconnection() = 0;

protected:
    log4cxx::LoggerPtr myLogger;


public:
    Wt::Dbo::backend::Postgres *postgres;// = new Wt::Dbo::backend::Postgres(connect_string);

    dbo::Session session;
    std::string dbname;




    ORMBase(const std::string & dbname) : PeLib::PeFileVisitor(), 
                                          dbname(dbname), processed(0)
    {
        if(!SRAND_CALLED){
            srand(time(NULL));
            SRAND_CALLED = true;
        }
        this->myLogger = getLogger(std::string("ORMBase"));
    }   
    
    virtual void callback(PeLib::PeFile32 &file);
    virtual void callback(PeLib::PeFile64 &file);
    void scan_and_process_directory_of_exes( std::string & start_dir);
    void init_database();
    
    ~ORMBase(){}


};



}
#endif
