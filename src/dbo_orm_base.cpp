/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
#include <Wt/Dbo/Dbo>
#include <Wt/WLogger>
#include <string>
#include <Wt/Dbo/Exception>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>

#include <stdlib.h> 
#include <cstring>

#include <algorithm>

#include <boost/filesystem.hpp>

#include <postgresql/libpq-fe.h>
#include <PeLib.h>
#include <vector>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "util.hpp"
#include "dbo_orm_base.hpp"
#include "dbo_orm_defs.hpp"

namespace dbo = Wt::Dbo;

namespace qfscanner{

boost::container::set<long> IMPORT_HASHES;
boost::container::set<long> EXPORT_HASHES;
boost::container::set<long> PEFILES_HASHES;
boost::container::set<long> EXPORTS_ASSOC_HASHES;
boost::container::set<long> IMPORTS_ASSOC_HASHES;

volatile bool SRAND_CALLED = false;

void ORMBase::init_hash_cache(  HASH_CACHE_TYPE x){
    std::string sql = "select hash_id    from ", table = "";
    boost::container::set<long> *the_hash_set = NULL;

    if (x == EXPORTS){
        table = EXPORTS_TABLE;
        the_hash_set = & (EXPORT_HASHES);
    }
    else if (x == IMPORTS){
        table = IMPORTS_TABLE;
        the_hash_set = & (IMPORT_HASHES);
    }
    else if (x == PEFILES){
        table = PEFILES_TABLE;
        the_hash_set = & (PEFILES_HASHES);
    }
    else if (x == IMPORTS_ASSOC){
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
        table = IMPORTS_ASSOC_TABLE;
    }
    else if (x == EXPORTS_ASSOC){
        the_hash_set = & (EXPORTS_ASSOC_HASHES);
        table = EXPORTS_ASSOC_TABLE;
    }


    std::string teh_query = sql + table + std::string(";");

    dbo::Transaction transaction_get_hashes(session);
    if (the_hash_set == NULL)
        return;
     
    Wt::Dbo::Query<long> query = session.query< long >(teh_query);
    

    Wt::Dbo::collection<long> results = query.resultList();
    Wt::Dbo::collection<long>::iterator it = results.begin();

    for (; it != results.end(); it++){
        the_hash_set->insert(*it);
    }


    std::stringstream ss_log;
    ss_log << "[+] Inserted: " << results.size() << " elements into the "<< table << " set."; 
    LOG4CXX_TRACE (this->myLogger, ss_log.str());


} 

bool ORMBase::check_hash_cache(  HASH_CACHE_TYPE x, long hash){
    boost::container::set<long> *the_hash_set = NULL;
    if (x == EXPORTS)
        the_hash_set = & (EXPORT_HASHES);
    else if (x == IMPORTS)
        the_hash_set = & (IMPORT_HASHES);
    else if (x == PEFILES)
        the_hash_set = & (PEFILES_HASHES);
    else if (x == IMPORTS_ASSOC)
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
    else if (x == EXPORTS_ASSOC)
        the_hash_set = & (EXPORTS_ASSOC_HASHES);

    if (the_hash_set == NULL)
        return false;

    return the_hash_set->find(hash) != the_hash_set->end();;

} 

void ORMBase::update_hash_cache(  HASH_CACHE_TYPE x, long hash){

    boost::container::set<long> *the_hash_set = NULL;
    if (x == EXPORTS)
        the_hash_set = & (EXPORT_HASHES);
    else if (x == IMPORTS)
        the_hash_set = & (IMPORT_HASHES);
    else if (x == PEFILES)
        the_hash_set = & (PEFILES_HASHES);
    else if (x == IMPORTS_ASSOC)
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
    else if (x == EXPORTS_ASSOC)
        the_hash_set = & (EXPORTS_ASSOC_HASHES);

    if (the_hash_set == NULL)
        return ;

    the_hash_set->insert(hash);

} 


void ORMBase::process_exports( dbo::ptr<PEFile> dbo_pefile,  
                              PEFile *pefilePtr,
                              PeLib::ExportDirectory& exp){
    
    std::string DllName = pefilePtr->FileName;

    dbo::Transaction process_exports_tran(this->session);

    std::vector< dbo::ptr<Export> > exportsVector;
    
    unsigned int numberOfExports =  exp.getNumberOfFunctions();

    boost::container::set<long> dbos_to_query;

    if (numberOfExports == 0)
        return;
    
    //dbo::Transaction transaction_add_export(session);
    // create check and update the exports    
    for (unsigned int i=0;i<exp.getNumberOfFunctions();i++)
    {
        std::string name =  exp.getFunctionName(i);
        Export *exportPtr = new Export(name, DllName);

        if (check_hash_cache(EXPORTS, exportPtr->hash_id)){
            dbos_to_query.insert(exportPtr->hash_id);
            delete exportPtr;
        }else{
            dbo::ptr<Export> dbo_export = session.add(exportPtr);
            update_hash_cache(EXPORTS, exportPtr->hash_id);
            exportsVector.push_back(dbo_export);
        }
        
    }
    
    LOG4CXX_TRACE (this->myLogger, std::string("Querying DB for existing exports."));
    boost::container::set<long>::iterator it;
    for(it = dbos_to_query.begin(); it != dbos_to_query.end(); it++){
        dbo::ptr<Export> dbo_export = session.find<Export>().where("hash_id = ?").bind(*it);
        exportsVector.push_back(dbo_export);
    }

    // update the exports association table
    LOG4CXX_TRACE (this->myLogger, std::string("Creating the export association in the DB, if they dont exist"));
    std::vector< dbo::ptr<Export> >::iterator it_exports = exportsVector.begin();
    for (; it_exports != exportsVector.end(); it_exports++){
        ExportAssociations *exportAssocPtr = new ExportAssociations(*(*it_exports), *pefilePtr);
        if (!check_hash_cache(EXPORTS_ASSOC, exportAssocPtr->hash_id)){
            dbo::ptr<ExportAssociations> dbo_export_assoc = session.add(exportAssocPtr);
            dbo_export_assoc.modify()->export_relation = *it_exports;
            dbo_export_assoc.modify()->pefile = dbo_pefile;
            update_hash_cache(EXPORTS_ASSOC, exportAssocPtr->hash_id);
        }   
    }
    process_exports_tran.commit();
    this->session.flush();
    //transaction_add_export.commit();
    LOG4CXX_TRACE (this->myLogger, std::string("Completed saving the exports to the DB"));
}



void ORMBase::process_pefile(PeLib::PeFile32 &file){
    
    PEFile *pefilePtr;
    dbo::ptr<PEFile> dbo_pefile;
    std::stringstream ss_log;

    ss_log << "[=] Processing file : " << file.getFileName();
    LOG4CXX_DEBUG (this->myLogger, ss_log.str());
    ss_log.str("");

    pefilePtr = new PEFile(file);
        
    dbo::Transaction create_pefile_tran(session);
    create_file_summary(pefilePtr);
    if (check_hash_cache(PEFILES, pefilePtr->hash_id)){
        dbo_pefile = session.find<PEFile>().where("hash_id = ?").bind(pefilePtr->hash_id);
        
        if (pefilePtr->Sha256 == dbo_pefile->Sha256)
        {
            delete pefilePtr;
            return;
        }
        
        ss_log << "[-] Found a duplicate hash for 32-bit : " << pefilePtr->FileName;
        LOG4CXX_INFO (this->myLogger, ss_log.str());
        ss_log.str("");
    }else{
        ss_log  << "[+] Added 32-bit ("<< pefilePtr->hash_id << ") " << pefilePtr->FileName;
        LOG4CXX_TRACE (this->myLogger, ss_log.str());
        ss_log.str("");

        update_hash_cache(PEFILES, pefilePtr->hash_id);

        dbo_pefile = session.add(pefilePtr);
        this->process_sections<32>(file, pefilePtr->hash_id);
        
        ss_log << "[+] Completed Sections processing for: " << dbo_pefile->FileName;
        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");


    }


    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash    
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Enumerating the exports"));
    if (pefilePtr->NumberOfExports > 0){
        // Since this file performs the exports, we will use it as the DllName
        PeLib::ExportDirectory& exp = dynamic_cast<PeLib::PeFile &>(file).expDir();
        process_exports(dbo_pefile, pefilePtr, exp);
    }
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Finished enumerating the exports"));
    
    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Enumerating the imports"));
    if (pefilePtr->NumberOfImportFiles > 0){
        process_imports<32>(file, dbo_pefile, pefilePtr);
    }
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Finished enumerating the imports"));
    create_pefile_tran.commit();
    this->session.flush();
    
}

void ORMBase::process_pefile(PeLib::PeFile64 &file){
    PEFile *pefilePtr;

    pefilePtr = new PEFile(file);
    dbo::ptr<PEFile> dbo_pefile;
    std::stringstream ss_log;
        
    dbo::Transaction create_pefile_tran(session);
    create_file_summary(pefilePtr);

    if (check_hash_cache(PEFILES, pefilePtr->hash_id)){
        dbo_pefile = session.find<PEFile>().where("hash_id = ?").bind(pefilePtr->hash_id);
        
        if (pefilePtr->Sha256 == dbo_pefile->Sha256)
        {
            delete pefilePtr;
            return;
        }
        
        ss_log << "[-] Found a duplicate hash for 64-bit : " << pefilePtr->FileName;
        LOG4CXX_INFO (this->myLogger, ss_log.str());
        ss_log.str("");
    }else{
        ss_log  << "[+] Added 64-bit ("<< pefilePtr->hash_id << ") " << pefilePtr->FileName;
        LOG4CXX_TRACE (this->myLogger, ss_log.str());
        ss_log.str("");

        update_hash_cache(PEFILES, pefilePtr->hash_id);

        dbo_pefile = session.add(pefilePtr);

        this->process_sections<64>(file, pefilePtr->hash_id);
        
        ss_log << "[+] Completed Sections processing for: " << dbo_pefile->FileName;
        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");
    }
    create_pefile_tran.commit();
    this->session.flush();


    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash    
    LOG4CXX_TRACE (this->myLogger, std::string("Enumerating the exports"));
    std::vector< dbo::ptr<Export> > exportsVector;
    if (pefilePtr->NumberOfExports > 0){
        // Since this file performs the exports, we will use it as the DllName
        PeLib::ExportDirectory& exp = dynamic_cast<PeLib::PeFile &>(file).expDir();
        process_exports(dbo_pefile, pefilePtr, exp);
    }
    LOG4CXX_TRACE (this->myLogger, std::string("Finished enumerating the exports"));

    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash
    LOG4CXX_TRACE (this->myLogger, std::string("Enumerating the imports"));
    if (pefilePtr->NumberOfImportFiles > 0){
        process_imports<64>(file, dbo_pefile, pefilePtr);
    }
    LOG4CXX_TRACE (this->myLogger, std::string("Finished enumerating the imports"));
    
    dbo::Transaction complete_processing(session);
    dbo_pefile.modify()->CompletedProcessing = true;
    complete_processing.commit();
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Completed processing PE"));
    
}


void ORMBase::scan_and_process_directory_of_exes( std::string & start_dir){
    
    boost::filesystem::path top_dir(start_dir);
    
    std::string sha256_hash = "";
    std::string md5_hash = "";
    std::stringstream ss_log;

    ss_log << std::hex << rand();
    this->badpefile_log = "pefile_files_failed_read_";
    this->badpefile_log += ss_log.str();
    this->badpefile_log += std::string(".log");
    ss_log.str("");
    ss_log <<  std::dec;
    
    // quick header indicating the directory scanned name.
    std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
    outfile << "# Scanning dir: " << start_dir << std::endl; 
    outfile.close();

    unsigned int files_processed = 0;
    for ( boost::filesystem::recursive_directory_iterator end, dir(top_dir); 
        dir != end; ++dir, ++files_processed) {
        std::string the_path = (*dir).path().native();
        if (the_path.find("/Microsoft/CryptnetUrlCache/Content/") != std::string::npos){
            std::string str_log = std::string( "[-] Skipping " ) + the_path + std::string(".  Might want to check it out.");
            LOG4CXX_WARN (this->myLogger, str_log);

        }

        if (boost::filesystem::is_directory(the_path))
            continue;

        bool exe_o_dll = is_exe_or_dll(the_path);
        if (!exe_o_dll){
            continue;
        }

        
        PeLib::PeFile* pef;
        try{
            pef = PeLib::openPeFile(the_path);
            if (pef == NULL){
                std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
                outfile << the_path << std::endl; 
                outfile.close();

                
                ss_log << "[-] Unable to parse filename (openPeFile returned NULL): " << the_path; 
                LOG4CXX_WARN (this->myLogger, ss_log.str());
                ss_log.str("");
                
                continue;    
            }
            pef->readMzHeader();
            pef->readPeHeader();
            pef->visit(*this);
            delete pef;
            
            ss_log << "[+] Processed ("<< files_processed << "): "<< the_path;
            LOG4CXX_TRACE (this->myLogger, ss_log.str());
            ss_log.str("");
        }catch(std::exception& e){
            std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
            outfile << the_path << std::endl; 
            outfile.close();

            ss_log << "[-] Unable to parse filename: " << the_path << std::endl; 
            ss_log << "\t[=] Got the following exception:\n" << e.what();

            LOG4CXX_WARN (this->myLogger, ss_log.str());
            ss_log.str("");
        }
        if (files_processed % 100 == 0){
            ss_log << "[+] Processed ("<< files_processed << ") files.";
            LOG4CXX_INFO (this->myLogger, ss_log.str());
            ss_log.str("");
        }
    }
}



void ORMBase::create_file_summary(PEFile *pefilePtr){
    dbo::Transaction transaction(session);
    dbo::ptr<FileSummary> dbo_fsumm;
    FileSummary *fsPtr = new FileSummary(pefilePtr);
    dbo_fsumm = session.add(fsPtr);
    transaction.commit();
}




void ORMBase::init_database(){
    
    try {
        dbo::Transaction setup(session);

        std::string str_log;
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + PEFILES_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<PEFile>(PEFILES_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + IMPORTS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Import>(IMPORTS_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + EXPORTS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Export>(EXPORTS_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + IMPORTS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<ImportAssociations>(IMPORTS_ASSOC_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + EXPORTS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<ExportAssociations>(EXPORTS_ASSOC_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + FILE_SUMMARY_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<FileSummary>(FILE_SUMMARY_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + SECTIONS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(SECTIONS_TABLE.c_str());
        
        str_log = std::string("[=] Creating the tables.");
        LOG4CXX_TRACE(this->myLogger, str_log);
        this->session.createTables();
        
        setup.commit();
        
        str_log = std::string("[+] Completed initialization of the tables.");
        LOG4CXX_INFO(this->myLogger, str_log);
        
    }catch(Wt::Dbo::Exception & e){

        std::string str_log = std::string("[=] Caught DB exception during initialization: ")+ e.what();
        LOG4CXX_WARN(this->myLogger, str_log);
        

        str_log = std::string("[=] Trying to use an existing database.");
        LOG4CXX_INFO(this->myLogger, str_log);
        init_hash_cache(EXPORTS);
        init_hash_cache(IMPORTS);
        init_hash_cache(PEFILES);
        //init_hash_cache(SECTIONS);
        init_hash_cache(IMPORTS_ASSOC);
        init_hash_cache(EXPORTS_ASSOC);

        str_log = "[+] Completed caching ORM DBO hash keys for processing.";
        LOG4CXX_INFO(this->myLogger, str_log);
        
        // need to cache all of the object hashes to ensure
        // expedited transactions and avoid creating objects
        // that already exist
    }
}

void ORMBase::callback(PeLib::PeFile32 &file){
    //std::cout << "Processing 32-bits" << std::endl;
    process_pefile(file);
}
void ORMBase::callback(PeLib::PeFile64 &file){
    //std::cout << "Processing 64-bits" << std::endl;
    process_pefile(file);
}
}