/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <Wt/Dbo/Dbo>
#include <Wt/WLogger>
#include <string>


#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>


#include <cstring>

#include <algorithm>
#include <string>
#include <boost/filesystem.hpp>

#include <postgresql/libpq-fe.h>
#include <PeLib.h>
#include <vector>

#include "util.hpp"
#include "dbo_orm_sqlite.hpp"
#include "dbo_orm_defs.hpp"


namespace dbo = Wt::Dbo;

namespace qfscanner{
    ORMSqlLite::ORMSqlLite(const std::string & dbname): ORMBase(dbname){
        this->init_dbconnection();
    }
    void ORMSqlLite::init_dbconnection(){
        this->sqlite3 = new dbo::backend::Sqlite3(this->dbname);
        this->session.setConnection(*sqlite3);
        this->init_database();        
    }
}