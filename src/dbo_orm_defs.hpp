/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <Wt/Dbo/Dbo>
#include <string>

#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/backend/Postgres>
#include <Wt/WString>

#include <boost/filesystem.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <openssl/sha.h>
#include <openssl/md5.h>

#include <magic.h>
#include <cstring>

#include <algorithm>
#include <string>

#include <PeLib.h>

#include "util.hpp"

#ifndef DBO_ORM_DEFS_H
#define DBO_ORM_DEFS_H

namespace dbo = Wt::Dbo;


namespace qfscanner{

class Import;
class Export;
class PEFile;
class ExportAssociations;
class ImportAssociations;
class FileSummary;
class Section;
}

namespace Wt{ namespace Dbo{

template<>
struct dbo_traits<qfscanner::Import> : public dbo_default_traits 
{
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

template<>
struct dbo_traits<qfscanner::Export> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

template<>
struct dbo_traits<qfscanner::PEFile> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<qfscanner::ImportAssociations> : public dbo_default_traits {
    typedef long IDType;
        static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<qfscanner::ExportAssociations> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

template<>
struct dbo_traits<qfscanner::FileSummary> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
};
template<>
struct dbo_traits<qfscanner::Section> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
};
}}

namespace qfscanner{

const std::string EXPORTS_TABLE = "exports";
const std::string IMPORTS_TABLE = "imports";
const std::string PEFILES_TABLE = "pefiles";
const std::string SECTIONS_TABLE = "sections";
const std::string IMPORTS_ASSOC_TABLE = "import_associations";
const std::string EXPORTS_ASSOC_TABLE = "export_associations";
const std::string FILE_SUMMARY_TABLE = "file_summaries";


class PEFile{
public:
    PEFile():
        Entropy(0.0), delayLoadedCnt(0), forwardedCnt(0), 
        Path(""), FileName(""), Extension(""), FileSize(0),
        NumberOfExports(0), NumberOfImportFiles(0), NumberOfImportFunctions(0),
        TimeDateStamp(0), NumberOfSections(0), SizeOfImage(0), SizeOfOptionalHeader(0),
        SizeOfCode(0), SizeOfInitializedData(0), SizeOfUninitializedData(0), 
        SizeOfHeaders(0), LoaderFlags(0), NumberOfRvaAndSizes(0), NumberOfSymbols(0),
        SizeOfStackReserve(0), SizeOfStackCommit(0), SizeOfHeapReserve(0), SizeOfHeapCommit(0),
        isSuspicious(false), reason(""), risk(0.0), Md5(""), 
        Sha256(""), hash_id(0), EntryPoint(0), SectionNameOEP("UNKNOWN") {}
    
    
    // exports and imports
    dbo::collection< dbo::ptr<ImportAssociations> > import_association;
    dbo::collection< dbo::ptr<ExportAssociations> > export_association;
    dbo::collection< dbo::ptr<Section> > sections;

    //dbo::collection< dbo::ptr<Section> > SectionOEP;
    // delay loaded dlls
    int delayLoadedCnt;
    // forwarded dlls
    int forwardedCnt;

    
    // number of exports imports
    int NumberOfExports;
    int NumberOfImportFiles;
    int NumberOfImportFunctions;

    
    // file information location information
    std::string Path;
    std::string FileName;
    long hash_id;
    std::string Extension;
    long diskSize;
    
    // PE Header Infos
    long EntryPoint;
    std::string SectionNameOEP;

    long TimeDateStamp;
    long NumberOfSections;
    long SizeOfImage;
    long SizeOfOptionalHeader;
    long SizeOfCode;
    long SizeOfInitializedData;
    long SizeOfUninitializedData;
    long SizeOfHeaders;
    long LoaderFlags;
    long NumberOfRvaAndSizes;
    long NumberOfSymbols;
    long SizeOfStackReserve;
    long SizeOfStackCommit;
    long SizeOfHeapReserve;
    long SizeOfHeapCommit;
    float Entropy;

    bool CompletedProcessing;
    long FileSize;
    bool isDll;
    int timestamp;
    // architecture
    int bits;
    
    // classification info    
    bool isSuspicious;
    std::string reason;
    float risk;

    // hashes 
    std::string Md5;
    std::string Sha256;

    template<int bits>
    void getImportInfo(PeLib::PeFileT<bits>& pef){
        this->NumberOfImportFunctions = 0;
        this->NumberOfImportFiles = 0;
        if (pef.readImportDirectory())
        {
            return;
        }
        const PeLib::ImportDirectory<bits>& imp = static_cast<PeLib::PeFileT<bits>&>(pef).impDir();
        //std::cout << "Reported number of files: "<< imp.getNumberOfFiles(PeLib::OLDDIR) << std::endl; 
        
        this->NumberOfImportFiles = imp.getNumberOfFiles(PeLib::OLDDIR);
        for (unsigned int i=0;i<imp.getNumberOfFiles(PeLib::OLDDIR);i++)
        {
            this->NumberOfImportFunctions += imp.getNumberOfFunctions(i, PeLib::OLDDIR);
        }
    }
    template<int bits>
    PEFile(PeLib::PeFileT<bits>& file):
            Entropy(0.0), delayLoadedCnt(0), forwardedCnt(0), EntryPoint(0),
            Path(""), FileName(""), Extension(""), FileSize(0),
            NumberOfExports(0), NumberOfImportFiles(0), NumberOfImportFunctions(0),
            TimeDateStamp(0), NumberOfSections(0), SizeOfImage(0), SizeOfOptionalHeader(0),
            SizeOfCode(0), SizeOfInitializedData(0), SizeOfUninitializedData(0), 
            SizeOfHeaders(0), LoaderFlags(0), NumberOfRvaAndSizes(0), NumberOfSymbols(0),
            SizeOfStackReserve(0), SizeOfStackCommit(0), SizeOfHeapReserve(0), SizeOfHeapCommit(0),
            isSuspicious(false), reason(""), risk(0.0), Md5(""), 
            Sha256(""), hash_id(0), isDll(false), SectionNameOEP("UNKNOWN"),
            CompletedProcessing(false){

        
        PeLib::PeHeaderT<bits>& peh = (file).peHeader();

        boost::filesystem::path file_path(file.getFileName().c_str());
        std::string filedata = "",
                    _path = file_path.string(); 
        
        
        this->Extension = file_path.extension().string();
        std::transform(this->Extension.begin(), this->Extension.end(), this->Extension.begin(), ::tolower);
        if (this->Extension.find(".") == 0){
            this->Extension.erase(0, 1);
        }

        this->FileName =  file_path.filename().string();
        this->Path = file_path.parent_path().string();
        this->FileSize = get_file_data(_path, filedata);
        float *entropy = calcEntropy(_path, 0, this->FileSize);
        if (entropy != NULL){
           this->Entropy =  *entropy;
           delete entropy; 
        }
       
        this->Md5 = calc_md5(filedata);
        this->Sha256 = calc_sha256(filedata);
        this->hash_id = long_hash_from_sha256_ascii(this->Sha256);

        this->bits = 64;
        
        this->NumberOfSections = peh.getNumberOfSymbols();
        this->TimeDateStamp = peh.getTimeDateStamp();
        this->NumberOfSections = peh.getNumberOfSections();
        this->SizeOfImage = peh.getSizeOfImage();
        this->SizeOfOptionalHeader = peh.getSizeOfOptionalHeader();
        this->SizeOfCode = peh.getSizeOfCode();
        this->SizeOfInitializedData = peh.getSizeOfInitializedData();
        this->SizeOfUninitializedData = peh.getSizeOfUninitializedData();
        this->SizeOfHeaders = peh.getSizeOfHeaders();
        this->SizeOfStackReserve = peh.getSizeOfStackReserve();
        this->LoaderFlags = peh.getLoaderFlags();
        this->NumberOfRvaAndSizes = peh.getNumberOfRvaAndSizes();
        this->NumberOfSymbols = peh.getNumberOfSymbols();
        this->SizeOfStackReserve = peh.getSizeOfStackReserve();
        this->SizeOfStackCommit = peh.getSizeOfStackCommit();
        this->SizeOfHeapReserve = peh.getSizeOfHeapReserve();
        this->SizeOfHeapCommit = peh.getSizeOfHeapCommit();
        this->isDll = false;

        this->EntryPoint = peh.getAddressOfEntryPoint();
        //int sectionNum = peh.
        //this->SectionNameOEP = peh.getSectionName();

        //getImportInfo<bits>(file, this);
        this->getImportInfo<bits>(file);
        if ((file).readExportDirectory()){
            // do nothing
        }else{
            PeLib::ExportDirectory& exp = dynamic_cast<PeLib::PeFile &>(file).expDir();
            this->NumberOfExports =  exp.getNumberOfFunctions();
            this->isDll = true;
        }

    }

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, hash_id, "hash_id");
        dbo::hasMany(a, export_association, dbo::ManyToOne, "Pefile");
        dbo::hasMany(a, import_association, dbo::ManyToOne, "Pefile");
        dbo::hasMany(a, sections, dbo::ManyToOne, "Pefile");

        // delay loaded dlls
        dbo::field(a, delayLoadedCnt, "DelayLoadedCnt");
        // forwarded dlls
        dbo::field(a, forwardedCnt, "ForwardedCnt");


        dbo::field(a, NumberOfImportFunctions, "NumberOfImportFunctions");
        dbo::field(a, NumberOfImportFiles, "NumberOfImportFiles");
        dbo::field(a, NumberOfExports, "NumberOfExports");

        // file information location information
        dbo::field(a, FileName, "FileName");
        dbo::field(a, Path, "FilePath");
        dbo::field(a, Extension, "Extension");
        dbo::field(a, FileSize, "FileSize");
        
        dbo::field(a, isDll, "isDll");
        dbo::field(a, bits, "bits");
        
        dbo::field(a, isSuspicious, "isSuspicious");
        dbo::field(a, reason, "Reason");

        dbo::field(a, Md5, "Md5");
        dbo::field(a, Sha256, "Sha256");
        dbo::field(a, Entropy, "Entropy");
        dbo::field(a, TimeDateStamp, "TimeDateStamp");
        dbo::field(a, NumberOfSections, "NumberOfSections");
        dbo::field(a, SizeOfImage, "SizeOfImage");
        dbo::field(a, SizeOfOptionalHeader, "SizeOfOptionalHeader");
        dbo::field(a, SizeOfCode, "SizeOfCode");
        dbo::field(a, SizeOfInitializedData, "SizeOfInitializedData");
        dbo::field(a, SizeOfUninitializedData, "SizeOfUninitializedData");
        dbo::field(a, SizeOfHeaders, "SizeOfHeaders");
        dbo::field(a, LoaderFlags, "LoaderFlags");
        dbo::field(a, NumberOfRvaAndSizes, "NumberOfRvaAndSizes");
        dbo::field(a, NumberOfSymbols, "NumberOfSymbols");
        dbo::field(a, SizeOfStackReserve, "SizeOfStackReserve");
        dbo::field(a, SizeOfStackCommit, "SizeOfStackCommit");
        dbo::field(a, SizeOfHeapReserve, "SizeOfHeapReserve");
        dbo::field(a, SizeOfHeapCommit, "SizeOfHeapCommit");


        dbo::field(a, EntryPoint, "EntryPoint");
        dbo::field(a, SectionNameOEP, "SectionNameOEP");
        dbo::field(a, CompletedProcessing, "CompletedProcessing");
        //dbo::hasMany(a, SectionOEP, "SectionOEP");
    }

};

class Export{
public:
    Export():SymbolName(""), DllName(""),hash_id(0){}
    
    Export(const std::string & SymbolName, const std::string& DllName):
        SymbolName(SymbolName.c_str(), SymbolName.size()), 
        DllName(DllName.c_str(), DllName.size()),
        parsingError(false), hexlified(""){
        std::string lower_dllname = this->DllName;
        std::transform(lower_dllname.begin(), lower_dllname.end(), lower_dllname.begin(), ::tolower);

        hexlified = hexlify(this->SymbolName);
        std::string printableName = getPrintableString(this->SymbolName);
        parsingError = printableName == SymbolName ? false : true;
        
        if(parsingError)
            this->SymbolName = printableName;

        this->hash_id = hashSymbols(lower_dllname, hexlified);


    }
    ~Export(){
        if (this->hash_id){
        /* left for debugging purposes */
        //    std::cout << "Destroying: " << std::hex << this->hash_id << " DllName: " << this->DllName;
        //    std::cout << " FunctionName: " << this->SymbolName << std::endl ;            
        }

    }

    dbo::collection< dbo::ptr<ExportAssociations> > export_association;
    
    std::string SymbolName;
    std::string DllName;
    long hash_id;
    bool parsingError;
    std::string hexlified;


    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, hash_id, "hash_id");
        dbo::hasMany(a, export_association, dbo::ManyToMany, "ExportRelations");
        dbo::field(a, SymbolName,     "SymbolName");
        dbo::field(a, DllName, "DllName");
        dbo::field(a, parsingError, "ParsingError");
        dbo::field(a, hexlified, "Hexlified");

    }
};



class Import{
public:
    Import(): SymbolName(""), DllName(""), hash_id(0){}

    Import(const std::string & SymbolName, const std::string & DllName):
        SymbolName(SymbolName.c_str(), SymbolName.size()), 
        DllName(DllName.c_str(), DllName.size()),
        parsingError(false), hexlified("") {
            std::string lower_dllname = this->DllName;
            std::transform(lower_dllname.begin(), lower_dllname.end(), lower_dllname.begin(), ::tolower);
            
            hexlified = hexlify(this->SymbolName);
            std::string printableName = getPrintableString(this->SymbolName);
            parsingError = printableName == SymbolName ? false : true;
            
            if(parsingError)
                this->SymbolName = printableName;
            
            this->hash_id = hashSymbols(lower_dllname, hexlified);

        }

    dbo::collection< dbo::ptr<ImportAssociations> > import_association;
    long hash_id;
    std::string SymbolName;
    std::string DllName;
    std::string hexlified;
    bool parsingError;

    ~Import(){
        if(this->hash_id){
        /* left for debugging purposes */
        //    std::cout << "Destroying: " << std::hex << this->hash_id << " DllName: " << this->DllName;
        //    std::cout << " FunctionName: " << this->SymbolName << std::endl;
        }
    }

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, hash_id, "hash_id");
        dbo::hasMany(a, import_association, dbo::ManyToMany, "ImportRelations");
        dbo::field(a, SymbolName,    "SymbolName");
        dbo::field(a, DllName, "DllName");   
        dbo::field(a, parsingError, "ParsingError");
        dbo::field(a, hexlified, "Hexlified");

    }
};


class ImportAssociations{
public:
    dbo::ptr<Import> import_relation;
    dbo::ptr<PEFile> pefile;

    ImportAssociations():
        risk(0.0), delayLoaded(false),
        forwarded(false), reason(""),
        isSuspicious(false){}
    
    ImportAssociations(const Import &imp, const PEFile &pef):
        risk(0.0), delayLoaded(false),
        forwarded(false), reason(""),
        isSuspicious(false)
    {
        this->hash_id = imp.hash_id ^ pef.hash_id;
    }
    
    long hash_id;
    float risk;;
    bool delayLoaded;
    bool forwarded;

    // classification info    
    bool isSuspicious;
    std::string reason;

    template<class Action>
    void persist(Action& a)
    {

        dbo::id(a, hash_id, "hash_id");
        
        dbo::belongsTo(a, pefile, "Pefile");
        dbo::belongsTo(a, import_relation, "ImportRelations");

        dbo::field(a, risk,     "Risk");
        dbo::field(a, delayLoaded, "DelayLoaded");
        dbo::field(a, forwarded, "Forwarded");

        dbo::field(a, isSuspicious, "IsSuspicious");
        dbo::field(a, reason, "Reason");

    }
};

class ExportAssociations{
public:
    dbo::ptr<Export> export_relation;
    dbo::ptr<PEFile> pefile;
    // other information about this particular pairing

    ExportAssociations():
        risk(0.0), delayLoaded(false),
        forwarded(false), isSuspicious(false),
        reason(""){}
    ExportAssociations(const Export &exp, const PEFile &pef):
        risk(0.0), delayLoaded(false),
        forwarded(false), reason(""),
        isSuspicious(false)
    {
        this->hash_id = pef.hash_id ^ exp.hash_id;
    }

    long hash_id;
    float risk;
    bool delayLoaded;
    bool forwarded;

    // classification info    
    bool isSuspicious;
    std::string reason;


    template<class Action>
    void persist(Action& a)
    {
        
        dbo::id(a, hash_id, "hash_id");

        dbo::belongsTo(a, pefile, "Pefile");
        dbo::belongsTo(a, export_relation, "ExportRelations");

        dbo::field(a, risk,     "Risk");
        dbo::field(a, delayLoaded, "DelayLoaded");
        dbo::field(a, forwarded, "Forwarded");

        dbo::field(a, isSuspicious, "IsSuspicious");
        dbo::field(a, reason, "Reason");


    }
};

class FileSummary{
public:
    std::string Sha256;
    std::string Md5;
    std::string FileName;
    std::string Path;
    std::string Extension;
    long hash_id;
    bool FailedProcessing;
    long FileSize;
    FileSummary():Sha256(""), Md5(""), FileName(""), 
            Path(""), FileSize(0), Extension(""), hash_id(0), FailedProcessing(true){}
    
    FileSummary(PEFile *ptr):Sha256(ptr->Sha256), Md5(ptr->Md5), FileName(ptr->FileName), 
            Path(ptr->Path), Extension(ptr->Extension), hash_id(ptr->hash_id), 
            FailedProcessing(false), FileSize(ptr->FileSize){}

    FileSummary(std::string filepath):Sha256(""), Md5(""), FileName(""), 
            Path(""), Extension(""), hash_id(0), FailedProcessing(true)
            {
                
                boost::filesystem::path file_path(filepath);

                this->Extension = file_path.extension().string();
                std::transform(this->Extension.begin(), this->Extension.end(), this->Extension.begin(), ::tolower);
                
                if (this->Extension.find(".") == 0){
                    this->Extension.erase(0, 1);
                }
                std::string filedata = "";
                this->FileSize = get_file_data(filepath, filedata);
                this->FileName =  file_path.filename().string();
                this->Path = file_path.parent_path().string();
                
                this->Md5 = calc_md5(filedata);
                this->Sha256 = calc_sha256(filedata);
                this->hash_id = long_hash_from_sha256((const unsigned char*) this->Sha256.c_str());

            }

    template<class Action>
    void persist(Action& a)
    {   
        //std::cout << "Persisting the hash value";
        dbo::field(a, hash_id, "hash_id");
        // file information location information
        //std::cout << "Persisting the FileName value";
        dbo::field(a, FileName, "FileName");
        //std::cout << "Persisting the FilePath value";
        dbo::field(a, Path, "FilePath");
        //std::cout << "Persisting the Extension value";
        dbo::field(a, Extension, "Extension");
        //std::cout << "Persisting the FailedProcessing value";
        dbo::field(a, FailedProcessing, "FailedProcessing");
        //std::cout << "Persisting the Md5 value";
        dbo::field(a, Md5, "Md5");
        //std::cout << "Persisting the Sha256 value";
        dbo::field(a, Sha256, "Sha256");
    }
};


class Section{
public:

    Section(): SectionName(""), Entropy(0.0), hash_id(0),
                VirtualSize(0), RawSize(0), 
                VirtualAddr(0), Characteristics(0){}

    Section(const std::string & PEName, 
        const std::string & SectionName, 
        long VirtualSize, long RawSize, 
        long VirtualAddr, float Entropy, long Characteristics):
        PEName(PEName),
        SectionName(SectionName),
        VirtualSize(VirtualSize), VirtualAddr(VirtualAddr),
        RawSize(RawSize), Entropy(Entropy), Characteristics(Characteristics),
        parsingError(false), hexlified(""), hash_id(0)
        {
            
            hexlified = hexlify(this->SectionName);
            std::string printableName = getPrintableString(this->SectionName);
            parsingError = printableName == SectionName ? false : true;
            
            if(parsingError)
                this->SectionName = printableName;
            
            // this may not be unique, but we are storing it for
            // analysis purposes
            hash_id = hashSymbols(hexlified);
        }

    dbo::ptr<PEFile> pefile;
    std::string SectionName;
    std::string PEName;
    long VirtualSize;
    long RawSize;
    long VirtualAddr;
    long Characteristics;
    float Entropy;
    long hash_id;
    bool parsingError;
    std::string hexlified;
    bool CompletedProcessing;

    ~Section(){
    }
    template<class Action>
    void persist(Action& a)
    {
        dbo::belongsTo(a, pefile, "Pefile");
        //dbo::belongsTo(a, SectionOEP, "SectionOEP");
        dbo::field(a, hash_id, "hash_id");
        dbo::field(a, PEName, "PEName");
        dbo::field(a, SectionName, "SectionName");
        dbo::field(a, VirtualAddr, "VirtualAddr");
        dbo::field(a, VirtualSize, "VirtualSize");
        dbo::field(a, RawSize, "RawSize");
        dbo::field(a, Entropy, "Entropy");
        dbo::field(a, Characteristics, "Characteristics");
        dbo::field(a, parsingError, "ParsingError");
        dbo::field(a, hexlified, "Hexlified");

    }
};






}
    


#endif
