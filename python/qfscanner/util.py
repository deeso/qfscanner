
import os
import apsw
import pefile
import hashlib
import magic
import base64
import sqlite3
import logging
import traceback


def is_exe_or_dll(path):
    f = path
    exe_o_dll = False
    if os.path.isdir(f):
        return False

    if len(os.path.splitext(f.lower()))  > 1:
        exe_o_dll = os.path.splitext(f.lower()) == 'exe' or \
                    os.path.splitext(f.lower()) == 'dll'

    # return true if we *think* its a dll
    if not exe_o_dll:
        buf = open(filename).read(5)            
        exe_o_dll = buf[:2] =='\x4D\x5A'
        del buf 

    if not exe_o_dll:
        MAGIC = magic.Magic()
        ty = MAGIC.id_filename(filename)
        ty = ty.lower()
        MAGIC.close()
        exe_o_dll = ty.find('pe') == 0 or \
                    ty.find("exe") > -1 or \
                    ty.find("sfx") > -1 or \
                    ty.find("code component") > -1 or \
                    ty.find("mscfu") > -1 or \
                    ty.find("ms-dos") > -1 or \
                    ty.find("installer") > -1 or \
                    ty.find("installer") > -1 or \
                    ty.find("self extracting") > -1 or \
                    ty.find("self-extracting") > -1 
            
    return exe_o_dll

def get_file_data(path):
    data = open(path).read()
    return len(data), data

def calc_sha256 (file_data):
    return hashlib.sha256(file_data).hexdigest()

def calc_md5 (file_data):
    return hashlib.md5(file_data).hexdigest()

def hashSymbols(one, two):
    concat = one+two
    sha256HashBuffer = hashlib.sha256(file_data).digest()
    return long_hash_from_sha256(sha256HashBuffer)

def long_hash_from_sha256(sha256HashBuffer):
    first8, second8, third8 = struct.unpack("<QQQ", sha256HashBuffer[:24]);
    return first8 ^ second8 ^ third8;
