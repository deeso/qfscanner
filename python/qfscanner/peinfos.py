

class PeFileInfos(object):
    def __init__(self, import_assoc, label=""):
        self.name = import_assoc.PEFile.FileName
        self.hash_id = import_assoc.Pefile_hash_id
        self.file_sz = import_assoc.PEFile.FileSize
        self.num_sections = import_assoc.PEFile.NumberOfSections
        self.num_imports = import_assoc.PEFile.NumberOfImportFunctions
        self.import_fns = {}
        self.import_files = set()
        self.label = label

    def add_import(self, import_assoc):

        import_file = import_assoc.Import.DllName.lower()
        if import_file.find('\n') > 0:
            import_file = import_file.split()[0]

        if import_file.find('.dll') == -1:
           import_file = import_file + '.dll' 

        self.import_files.add(import_file.lower().strip())
        self.import_fns.setdefault(import_file.lower().strip(), []).append(import_assoc.Import.SymbolName)

    def __str__(self):
        return "%s:%d: %d ImportFiles %d ImportFns %d Sections"%(self.name, self.hash_id,
                                                                 len(self.import_files), self.num_imports,
                                                                 self.num_sections)
    def stringify_imports(self):
        results = []
        for f in self.import_fns.keys():
            for fn in self.import_fns[f]:
                results.append("%s:%s"%(f, fn))
        return "\n".join(results)

    def vector_dll_csv(self, dll_names = []):
        return ",".join(self.get_dll_vector(dll_names=dll_names))

    def vector_csv(self, dll_names = []):
        return ",".join(self.get_dll_vector(dll_names=dll_names))
    
    def get_dll_dict(self, dll_names = []):
        return dict([('hash_id', self.hash_id), 
               ('file_sz', self.file_sz), 
               ('num_imports', self.num_imports), 
               ('num_sections', self.num_sections)] +
              [(k, len(self.import_fns.get(k, []))) for k in dll_names])

    def get_dll_vector(self, dll_names = []):
        return [len(self.import_fns.get(k, [])) for k in dll_names]


    def get_vector(self, dll_names = []):
        return [self.hash_id, self.file_sz, self.num_imports, self.num_sections] +\
            [len(self.import_fns.get(k, [])) for k in dll_names]

    def get_dll_vector_features(self, dll_names):
        return ['hash_id', 'pe_size', 'num_imports', 'num_sections'] + dll_names

    def get_vector_features(self, dll_names):
        return dll_names

    def dll_fns(self, dll_file):
        return set(self.import_fns.setdefault(dll_file, []))

    def dll_fns_vectors(self, dll_file, dll_fns=[]):
        
        if not dll_file in self.import_fns and len(dll_fns) == 0:
            return [0 for fn in dll_fns]

        elif not dll_file in self.import_fns:
            return []

        elif len(dll_fns) == 0:
            return [1 for fn in self.import_fns[dll_file]]

        return [ 1 if fn in self.import_fns[dll_file] else 0 for fn in dll_fns]
    
    def dll_fns_dict(self, dll_file, dll_fns=[]):
        
        if not dll_file in self.import_fns and len(dll_fns) == 0:
            return dict( [(fn, 0) for fn in dll_fns])

        elif not dll_file in self.import_fns:
            return dict( )

        elif len(dll_fns) == 0:
            return dict( [(fn, 1) for fn in dll_fns])

        return dict( [(fn, 1 if fn in self.import_fns[dll_file] else 0) for fn in dll_fns]) 

    def dll_files(self):
        return self.import_files

