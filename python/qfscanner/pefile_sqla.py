import os
import apsw
import pefile
import hashlib
import magic
import base64
import sqlite3
import logging
import traceback


from datetime import datetime
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import sessionmaker 
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine, ForeignKey, pool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, Column, Date, Integer, String, Table, Boolean
from sqlalchemy import select, and_

loggers = {}
def get_my_logger(name, lvl):
    global loggers
    if name in loggers:
        log = loggers[name]
        log.setLevel(lvl)
        return log 

    formatter = logging.Formatter( \
            '[%(levelname)s] [%(asctime)s] *%(name)s* %(message)s', \
            "%Y-%m-%d %H:%M:%S")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    log = logging.getLogger(name)
    log.setLevel(lvl)
    log.addHandler(handler)
    loggers[name] = log
    return log

class SQLiteBackend(object):
    @classmethod
    def setup_backend(cls, init_string, **kargs):

        engine = create_engine(init_string, **kargs)
        Session = sessionmaker(bind=engine)
        return engine, Session
        
    @classmethod
    def backup_sqlite_strings_conn(cls, conn, db):
        new_conn = apsw.Connection(db)
        with new_conn.backup("main", conn, "main") as backup:
            backup.step() # copy whole database in one go
        new_conn.close()

jpath = lambda p, c: os.path.join(p, c)
MAGIC = magic.Magic()
class PEEntryUtil(object):
    @classmethod
    def parse_pefile(cls, session, filename, debug_lvl=logging.INFO, logger=None):
        pe = None
        i_set_logger = False
        if logger is None:
            logger = get_my_logger("PEEntryUtil.parse_pefile", debug_lvl)
            i_set_logger = True
        
        try:
            pe = pefile.PE(filename)
        except:
            logger.warn("Not a valid PEfile: %s"%filename)
            if i_set_logger:
                del logger
            return None

        path, name = os.path.split(filename)

        pe_ts = pe.FILE_HEADER.TimeDateStamp
        timestamp = datetime.fromtimestamp(pe_ts)
        sha512 = hashlib.sha512(open(filename).read()).hexdigest()
        statinfo = os.stat(filename )
        sz = statinfo.st_size
        pe_row = None
        
        #try:
        #    pe_row = session.query(PE).\
        #            filter( PE.sha512 == sha512, \
        #                    PE.name == name, \
        #                    PE.path == path).one()
        #    logger.info("File already processed: (%s) %s "%(sha512, filename))
        #    return
        #except NoResultFound as nrf:
        #    pe_row = PE(path, name, sha512, timestamp, sz, is_dll=pe.is_dll())
        
        pe_row = PE(path, name, sha512, timestamp, sz, is_dll=pe.is_dll())
        

        session.add(pe_row)
        session.commit()

        if pe.is_dll() and hasattr(pe, 'DIRECTORY_ENTRY_EXPORT'):
            dllname = name
            exports = []
            # read each directory entry
            for symbol in pe.DIRECTORY_ENTRY_EXPORT.symbols:
                #for symbol in symbols:
                sym_name = symbol.name
                if sym_name is None:
                    continue

                row = None
                try:
                    row = session.query(Export).\
                            filter( Export.name == sym_name, Export.dllname == dllname).one()
                except NoResultFound as nrf:
                    pass
                
                if row is None:
                    row = Export( sym_name, dllname)
                
                row.exports_cnt += 1
                # add the PE file to the exporting_pes since
                # it is backref'ed to the PE
                row.exporting_pes.append(pe_row)            
                session.commit()
                del row

            logger.debug("Processing exports completed.")


        # read each directory entry
        imports = []
        if hasattr(pe, 'DIRECTORY_ENTRY_IMPORT'):    
            for dll in pe.DIRECTORY_ENTRY_IMPORT:
                dllname = dll.dll
                
                for i in dll.imports:
                    sym_name = i.name
                    
                    if sym_name is None:
                        continue
                    
                    row = None
                    try:
                        row = session.query(Import).\
                            filter( Import.name == sym_name, Import.dllname == dllname).one()

                    except NoResultFound as nrf:
                        pass                    

                    if row is None:
                        #print sym_name, dllname
                        row = Import( sym_name, dllname)
                        session.add(row)
                        session.commit()

                    row.imports_cnt += 1
                    # add the PE file to the importing_pes since
                    # it is backref'ed to the PE
                    row.importing_pes.append(pe_row)      
                    session.commit()
                    del row
            
            logger.debug("Processing imports completed.")

        
        session.commit()
        logger.debug("Completed processing %s."%filename)
        if i_set_logger:
            del logger

        return pe_row
    
    @classmethod
    def get_files_paths(cls,  directory, debug_lvl=logging.INFO, logger=None):
        global MAGIC
        i_set_logger = False
        if logger is None:
            logger = get_my_logger("PEEntryUtil", debug_lvl)
            i_set_logger = True
 
        files = [jpath(directory, i) for i in  os.listdir(directory)]
        paths = []
        #print files
        for i in files:
            #print i, os.path.isdir(i)
            #print os.path.splitext( i )[-1], os.path.splitext( i )[-1] == '.apk'
            if os.path.isdir(i):
                logger.debug ("Walking down %s"%i)
                paths += cls.get_files_paths( i, logger=logger)
            else:
                if PEEntryUtil.is_exe_or_dll(i):
                    paths.append(i)
                    logger.debug ("added path: %s"%i )   
        
        if i_set_logger:
            del logger

        return paths

    @classmethod
    def is_exe_or_dll(cls, filename, debug_lvl=logging.INFO, logger=None):
        
        i_set_logger = False
        if logger is None:
            logger = get_my_logger("is_exe_or_dll", debug_lvl)
            i_set_logger = True

        f = filename
        # check if its a valid file
        if os.path.isdir(f):
            logger.debug("%s not a valid PE file (its a dir)"%f)
            return False

        # is this a valid file we can read?
        try:
            os.stat(f)
        except:
            logger.debug("%s is not a valid PE file (not a valid file)"%f)
            return False

        # does it have an exe or dll extension?
        exe_o_dll = False
        if len(os.path.splitext(f.lower()))  > 1:
            exe_o_dll = os.path.splitext(f.lower()) == 'exe' or \
                        os.path.splitext(f.lower()) == 'dll'
        
        # return true if we *think* its a dll
        if not exe_o_dll:
            buf = open(filename).read(5)            
            exe_o_dll = buf[:2] =='\x4D\x5A'
            del buf 

    
        if not exe_o_dll:
            MAGIC = magic.Magic()
            ty = MAGIC.id_filename(filename)
            MAGIC.close()
            exe_o_dll = ty.find('pe') == 0 or \
                        ty.find("exe") > -1 or \
                        ty.find("sfx") > -1 or \
                        ty.find("code component") > -1 or \
                        ty.find("mscfu") > -1 or \
                        ty.find("ms-dos") > -1 or \
                        ty.find("installer") > -1 or \
                        ty.find("installer") > -1 or \
                        ty.find("self extracting") > -1 or \
                        ty.find("self-extracting") > -1 
        
        if not exe_o_dll: 
            logger.debug("%s is not a valid PE file"%f)
        else:
            logger.debug("%s IS a VALID PE file"%f)
            
        return exe_o_dll

Base = declarative_base()




class PE(Base):
    __tablename__ = "pes"
 
    id = Column(Integer, unique=True, primary_key=True)
    name = Column(String)
    path = Column(String)
    size = Column(Integer)

    sha512 = Column(String)
    timestamp = Column(Date)
    filetype = Column(String)
    
    is_dll = Column(Boolean)

    def __init__(self, path, name, sha512, timestamp, sz, is_dll=False):
        """"""
        self.size = sz
        self.path = path
        self.name = name
        self.sha512 = sha512
        self.timestamp = timestamp
        self.is_dll = is_dll
    
    def add_imports(self, import_list=[]):
        for i in import_list:
            self.imports.append(i)

    def add_exports(self, export_list=[]):
        for i in export_list:
            self.exports.append(i)

imports_association_table = Table('imports_association', Base.metadata,
   Column('imports_id', Integer, ForeignKey('imports.id')),
   Column('pe_id', Integer, ForeignKey('pes.id'))
)

exports_association_table = Table('exports_association', Base.metadata,
   Column('exports_id', Integer, ForeignKey('exports.id')),
   Column('pes_id', Integer, ForeignKey('pes.id'))
)

def manually_query_association_count(pe_id, session):
    conn = session.connection().connect()
    qry = select([func.count('*')],
        and_(imports_association_table.columns.pe_id == pe_id,
             Import.id == imports_association_table.columns.imports_id
            )
        )
    return conn.execute(qry).scalar()

# class ExportAssociations(Base):
#     __tablename__ = "exports_associations"
#     id = Column(Integer, primary_key=True)
#     exports_id = Column(Integer, ForeignKey('exports.id'))
#     pes_id = Column(Integer, ForeignKey('pes.id'))



# class ImportAssociations(Base):
#     __tablename__ = "imports_associations"
#     id = Column(Integer, primary_key=True)
#     imports_id = Column(Integer, ForeignKey('imports.id'))
#     pes_id = Column(Integer, ForeignKey('pes.id'))

class Export(Base):
    __tablename__ = "exports"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)  
    dllname = Column(String)
    exports_cnt  = Column(Integer)
    exporting_pes = relationship("PE",
                    secondary=exports_association_table,
                    backref="exports")

    def __init__(self, name, dllname, occurences = 0, **kargs):
        """"""
        self.name = name
        self.dllname = dllname
        self.exports_cnt = 0


class Import(Base):
    __tablename__ = "imports"
 
    id = Column(Integer, primary_key=True)
    dllname = Column(String)
    name = Column(String)  
    imports_cnt = Column(Integer) 
    #importing_pes = relationship("PE",
    #                secondary=imports_association_table,
    #                backref=backref("imports", uselist=False))
    importing_pes = relationship("PE",
                    secondary=imports_association_table,
                    backref='imports')
    
    def __init__(self, name, dllname, occurences = 0, **kargs):
        """"""
        self.name = name
        self.dllname = dllname
        self.imports_cnt = 0





class PEAnalysisSQLiteFE(object):
    def __init__(self, connection=None, uri='sqlite:///:memory:', debug_lvl=logging.INFO, **kargs):
        self.connection = apsw.Connection(':memory:')
        self.pool = pool.SingletonThreadPool (lambda: sqlite3.connect(self.connection))

        self.engine, self.Session = SQLiteBackend.setup_backend(uri, pool=self.pool, **kargs)
        Base.metadata.create_all(self.engine)
        self.session = self.Session()
        self.logger = get_my_logger("PEAnalysisSQLiteFE", debug_lvl)
    


    def add_pe_file(self, pe_file):
        pe = PEEntryUtil.parse_pefile(self.session, pe_file)
        return pe

    def process_directory(self, directory, dbbackup, interval=500):
        process_cnt = 0
        files = PEEntryUtil.get_files_paths(directory)
        self.process_filelist(files, dbbackup, interval=interval)

    def process_filelist(self, files, dbbackup, interval=500):
        process_cnt = 0
        
        pending_writes = []
        out = open("tmp_notes.txt", 'w')
        completed = open("completed.txt", 'w')
        for f in files:
            
            if f.find('/media/dd_img/Users/alball/AppData/LocalLow/Microsoft/CryptnetUrlCache/') > -1:
                continue

            if not PEEntryUtil.is_exe_or_dll(f):
                continue

            #print "Handling %s"%f
            self.add_pe_file(f)
            process_cnt += 1
            
            if process_cnt % interval == 0:
                SQLiteBackend.backup_sqlite_strings_conn(self.connection, dbbackup)
                completed.write('\n'.join(pending_writes))
                self.logger.debug ("*************Processed %d files."%(process_cnt))
        
        SQLiteBackend.backup_sqlite_strings_conn(self.connection, dbbackup)
        completed.write('\n'.join(pending_writes))
        self.logger.debug ("Completed processing %d files."%(process_cnt))


class PEAnalysisPostGresFE(object):
    def __init__(self, user, password, hostname="localhost", dbname='pescan', 
            create_db=False, create_all=False, debug_lvl=logging.INFO, **kargs):

        uri_args = {'user':user, 'password':password, 'hostname':hostname}
        self.uri = "postgres://{user}:{password}@{hostname}".format(**uri_args)

        uri_args['dbname'] = dbname
        self.db_uri = "postgres://{user}:{password}@{hostname}/{dbname}".format(**uri_args)
        
        self.engine = create_engine(self.db_uri, **kargs)
        self.logger = get_my_logger("GenPostGresDefaults", debug_lvl)
        
        if create_db:
            self.engine = create_engine(self.uri, **kargs)
            self.create_db(dbname)
            self.engine = create_engine(self.db_uri, **kargs) 
            create_all = True

        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
        
        if create_all:
            self.drop_all()
            Base.metadata.create_all(self.engine)

    def create_db(self, dbname):
        conn = self.engine.connect()
        conn.execute('commit')
        try:
            conn.execute("create database %s"%dbname)
        except:
            self.logger.warn(traceback.format_exc())
        conn.close()
         
    def drop_all(self):
        meta = MetaData(self.engine)
        meta.reflect()
        try:
            meta.drop_all()
        except:
            self.logger.warn(traceback.format_exc())    

    def add_pe_file(self, pe_file):
        pe = PEEntryUtil.parse_pefile(self.session, pe_file)
        return pe

    def process_directory(self, directory, dbbackup, interval=500):
        process_cnt = 0
        files = PEEntryUtil.get_files_paths(directory)
        self.process_filelist(files, dbbackup, interval=interval)


    def process_filelist(self, files, interval = 20):
        process_cnt = 0
        
        out = open("tmp_notes.txt", 'w')
        completed = open("completed.txt", 'w')
        for f in files:
            if f.find('/media/dd_img/Users/alball/AppData/LocalLow/Microsoft/CryptnetUrlCache/') > -1:
                continue

            if not PEEntryUtil.is_exe_or_dll(f):
                continue

            #print "Handling %s"%f
            self.add_pe_file(f)
            completed.write(f+'\n')
            process_cnt += 1
            if process_cnt % interval == 0:
                self.logger.info ("*************Processed %d files."%(process_cnt))        
        
        self.logger.info ("Completed processing %d files."%(process_cnt))
        

class CompareImportsExports(object):
    def __init__(self, session):
        self.imports = {}
        self.dlls = {}
        self.export = set()
        self.imports_cnt = 0
        self.exports_cnt = 0
        self.init_imports(session)

    
    def init_imports(self, session, cnt=1):
        imports = session.query(Import).all()
        for sym in imports:
            if not sym.name in self.imports:
                self.imports[sym.dllname] = DLLImports(sym.dllname)

            self.imports[sym.name].add_sym(sym.name)
            self.imports_cnt = 0


    def init_exports(self, session):
        exports = session.query(Export).all()
        for export_ in exports:
            self.add_export(import_.name)


    def add_import(self, dllname, import_sym):
        if not dllname in self.dlls:
            self.dlls[dllname] = DLLImports(dllname)
        self.dlls[dllname].add_sym(import_sym)
        self.import_cnt += 1  



class DLLImports(object):
    def __init__(self, dllname):
        self.dllname = dllname
        self.imports = {}
        self.active = 0

    def add_sym(self, name):
        self.active += 1
        if not name in imports:
            self.imports[name] = 0 
        self.imports[name] += 1



dbbackup = '/home/dso/exes_info.db'
files = [i.strip()  for i in open("/media/truecrypt1/energy_disk/files_ntfs2.txt").read().splitlines() if len(i.strip()) > 0]


def paged_imports_query(q):
    offset = 0
    while True:
        r = False
        for elem in q.limit(10).offset(offset):
           r = True
           yield elem
        offset += 1000
        if not r:
            break

#query = run.session.query(func.count(PE.imports)).filter(PE.isdll).group_by(PE.name)
#items = []

#for item in imports_query(query):
#    print item
# commands for 
#CREATE USER pescanner WITH PASSWORD '';
#CREATE DATABASE pescanner;
#GRANT ALL PRIVILEGES ON DATABASE pescanner TO pescanner;

def do_sqlite(files_lst, dbbackup, echo=False, backup_interval=1000):
    run = PEAnalysisSQLiteFE(echo=echo)
    try:
        run.process_filelist(files, dbbackup, backup_interval=backup_interval)
    except:
        print traceback.format_exc()
    return run

def do_postgres_sql(username, password, dbname, files_lst, echo=False, backup_interval=1000):
    run = PEAnalysisPostGresFE(uri, echo=echo)
    try:
        run.process_filelist(files, dbbackup, backup_interval=backup_interval)
    except:
        print traceback.format_exc()
    return run

