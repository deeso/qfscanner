import sys
qfscanner_python = '/research_data/code/git/qfscanner/python/'
sys.path.append(qfscanner_python)
from qfscanner import dbo_orm_defs
from qfscanner.peinfos import PeFileInfos


from mvpa2.clfs.svm import LinearCSVMC
from mvpa2.clfs.knn import kNN
from mvpa2.datasets import dataset_wizard
from mvpa2.datasets.base import Dataset
from mvpa2.base.collections import ArrayCollectable, SampleAttribute
from mvpa2.clfs.distance import one_minus_correlation
from mvpa2.measures.base import CrossValidation
from mvpa2.generators.partition import HalfPartitioner, NFoldPartitioner

import matplotlib.pyplot as plt

import binascii,string

import pandas as pd
import numpy as np
import pylab as pl
import logging                          
import unicodedata, re
from sklearn import preprocessing
import operator



def create_dataset(data_values, dlls_of_interest, K_fold = 5):
    features = dlls_of_interest 
    # normaliing the number imports from each of the files vs the number of imports 
    data = np.array([np.array(i.get_dll_vector(dlls_of_interest), dtype=np.float)/(i.num_imports)  for i in data_values.values()])
    target = np.array([1 if i.label == 'gui' else 0  for i in data_values.values()])
    chunks = np.array([i for i in xrange(0, data.shape[0])]) % K_fold
    # convert scikit Bunch
    
    target_set_collectable = ArrayCollectable(target, name='targets')
    ds = dataset_wizard(data, targets=target_set_collectable)
    #ds.sa['Filename'] = SampleAttribute(sample_set[:, 0])
    #ds.sa['hash_id'] = SampleAttribute(sample_set[:, 1])
    #ds.sa['Partitioner'] = SampleAttribute(sample_set[:, 2])

    ds.sa['Filename'] = np.array([i.name   for i in data_values.values()])
    ds.sa['chunks'] = chunks
    return ds


def perform_quick_learn(data_values, dlls_of_interest):
    ds = create_dataset(data_values, dlls_of_interest, 5)
    clf = LinearCSVMC()
    cvte = CrossValidation(clf, NFoldPartitioner(), errorfx=lambda p, t: np.mean(p == t), enable_ca=['stats'])
    cv_results = cvte(ds)
    np.mean(cv_results)

    print cvte.ca.stats.as_string(description=True)
    print cvte.ca.stats.matrix


gui_malware_run =  dbo_orm_defs.PostGresFE('pescanner', 'knBGbdkx2QCyBhgp85NB', dbname='virusshare_gui_malware')
non_gui_run = dbo_orm_defs.PostGresFE('pescanner', 'knBGbdkx2QCyBhgp85NB', dbname='cygwin_nogui_clean')

non_gui_imports = non_gui_run.session.query(dbo_orm_defs.ImportAssociations).all()
gui_malware_imports = gui_malware_run.session.query(dbo_orm_defs.ImportAssociations).all()

gui_values = {}
for import_assoc in gui_malware_imports:
    gui_values.setdefault(import_assoc.Pefile_hash_id, PeFileInfos(import_assoc)).add_import(import_assoc)


nongui_values = {}
for import_assoc in non_gui_imports:
    nongui_values.setdefault(import_assoc.Pefile_hash_id, PeFileInfos(import_assoc)).add_import(import_assoc)

data_values = {}
for import_assoc in gui_malware_imports:
    data_values.setdefault(import_assoc.Pefile_hash_id, PeFileInfos(import_assoc, label="gui")).add_import(import_assoc)

for import_assoc in non_gui_imports:
    data_values.setdefault(import_assoc.Pefile_hash_id, PeFileInfos(import_assoc, label="nongui")).add_import(import_assoc)




dlls_of_interest = '''advapi32.dll
avifil32.dll
comctl32.dll
comdlg32.dll
duser.dll
gdi32.dll
gdiplus.dll
imm32.dll
kernel32.dll
mfc42.dll
mscms.dll
msdart.dll
msimg32.dll
msvbvm50.dll
msvbvm60.dll
ntdll.dll
ole32.dll
oleaut32.dll
oledb32.dll
setupapi.dll
shell32.dll
user32.dll
winmm.dll'''.split()


perform_quick_learn(data_values, dlls_of_interest)


dlls_of_interest = '''comctl32.dll
comdlg32.dll
gdi32.dll
gdiplus.dll
msvbvm50.dll
msvbvm60.dll
ole32.dll
setupapi.dll
user32.dll'''.split()

perform_quick_learn(data_values, dlls_of_interest)





dlls_of_interest = '''comdlg32.dll
gdi32.dll
gdiplus.dll
msvbvm50.dll
msvbvm60.dll
ole32.dll
setupapi.dll
user32.dll'''.split()

perform_quick_learn(data_values, dlls_of_interest)

# After running these three experiments, it turns out that having more DLL information is necessary for better accuracy and precision



dlls_of_interest = '''advapi32.dll
avifil32.dll
comctl32.dll
comdlg32.dll
duser.dll
gdi32.dll
gdiplus.dll
imm32.dll
kernel32.dll
mfc42.dll
mscms.dll
msdart.dll
msi.dll
msimg32.dll
msvbvm50.dll
msvbvm60.dll
ntdll.dll
ole32.dll
oleaut32.dll
oledlg.dll
oledb32.dll
setupapi.dll
shell32.dll
user32.dll
winmm.dll'''.split()
perform_quick_learn(data_values, dlls_of_interest)

# no improvement after adding more DLL files for comparison

dlls_of_interest = '''advapi32.dll
avifil32.dll
comctl32.dll
comdlg32.dll
duser.dll
gdi32.dll
gdiplus.dll
imm32.dll
mfc42.dll
mscms.dll
msdart.dll
msi.dll
msimg32.dll
msvbvm50.dll
msvbvm60.dll
ole32.dll
oleaut32.dll
oledlg.dll
oledb32.dll
setupapi.dll
shell32.dll
user32.dll
winmm.dll'''.split()
perform_quick_learn(data_values, dlls_of_interest)














# create the X from our data
# note X does not need to be scaled here
X = data.copy()
Y = target.copy()

y = Y
clf = svm.SVC(kernel='linear')
clf.fit(X, Y)
y_pred = clf.predict(X)
classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
print("classif_rate for %s : %f " % ("SVM", classif_rate))

# <codecell>

# get the separating hyperplane
w = clf.coef_[0]
a = -w[0] / w[1]
xx = np.linspace(-5, 5)
yy = a * xx - (clf.intercept_[0]) / w[1]


# plot the parallels to the separating hyperplane that pass through the
# support vectors
b = clf.support_vectors_[0]
yy_down = a * xx + (b[1] - a * b[0])
b = clf.support_vectors_[-1]
yy_up = a * xx + (b[1] - a * b[0])

# plot the line, the points, and the nearest vectors to the plane
pl.plot(xx, yy, 'k-')
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)

pl.axis('tight')
pl.show()

# <codecell>

n_features = X.shape[1]

C = 1.0

# Create different classifiers. The logistic regression cannot do
# multiclass out of the box.
classifiers = {'L1 logistic': LogisticRegression(C=C, penalty='l1'),
               'L2 logistic': LogisticRegression(C=C, penalty='l2'),
               'Linear SVC': SVC(kernel='linear', C=C, probability=True)}

n_classifiers = len(classifiers)

pl.figure(figsize=(3 * 2, n_classifiers * 2))
pl.subplots_adjust(bottom=.2, top=.95)

for index, (name, classifier) in enumerate(classifiers.iteritems()):
    classifier.fit(X_scaled, y)

    y_pred = classifier.predict(X)
    classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
    print("classif_rate for %s : %f " % (name, classif_rate))

    # View probabilities=
    xx = np.linspace(3, 9, 100)
    yy = np.linspace(1, 5, 100).T
    xx, yy = np.meshgrid(xx, yy)
    Xfull = np.c_[xx.ravel(), yy.ravel()]
    probas = classifier.predict_proba(Xfull)
    n_classes = np.unique(y_pred).size
    for k in range(n_classes):
        pl.subplot(n_classifiers, n_classes, index * n_classes + k + 1)
        pl.title("Class %d" % k)
        if k == 0:
            pl.ylabel(name)
        imshow_handle = pl.imshow(probas[:, k].reshape((100, 100)),
                                  extent=(3, 9, 1, 5), origin='lower')
        pl.xticks(())
        pl.yticks(())
        idx = (y_pred == k)
        if idx.any():
            pl.scatter(X[idx, 0], X[idx, 1], marker='o', c='k')

# <codecell>

b

# <codecell>

ax = pl.axes([0.15, 0.04, 0.7, 0.05])
pl.title("Probability")
pl.colorbar(imshow_handle, cax=ax, orientation='horizontal')

pl.show()

# <codecell>

dlls_of_interest = '''comctl32.dll
comdlg32.dll
gdi32.dll
gdiplus.dll
msvbvm50.dll
msvbvm60.dll
ole32.dll
setupapi.dll
user32.dll'''.split()

features = [ 'hash_id', 'pe_size', 'num_imports', 'num_sections'] + dlls_of_interest 

gui_vectors = [i.get_dll_vector(dlls_of_interest) for i in gui_values.values()]
nongui_vectors = [ i.get_dll_vector(dlls_of_interest) for i in nongui_values.values()]

target_vector = ["gui" for i in xrange(0, len(gui_vectors))] +\
                ["non_gui" for i in xrange(0, len(nongui_vectors))]

data_vectors = gui_vectors + nongui_vectors
# convert scikit Bunch

n_features = len(dlls_of_interest) # + len(['num_imports', 'num_sections']) 
n_samples = len(data_vectors)

target_names = ['gui', 'non_gui']
target = np.empty((n_samples,), dtype=np.int)
data = np.empty((n_samples, n_features))

# ignore hash_id and file size
for pos, values in enumerate(data_vectors):
    data[pos] = np.asarray(values[4:], dtype=np.float)
    target[pos] = np.asarray((1 if target_vector[pos] == 'gui' else 0), dtype=np.int)




chunks = sample_set2[:, 1].copy()
chunks = chunks % K_fold

gui_bunch = Bunch(data=data, target=target,
             target_names=target_names,
             feature_names=features[4:])

# <codecell>

# learn using sklearn's svm library
from sklearn import svm
X = data.copy()
Y = target.copy()
X_scaled = scale(X)

y = Y
clf = svm.SVC(kernel='linear')
clf.fit(X_scaled, Y)

y_pred = clf.predict(X)
classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
print("classif_rate for %s : %f " % ("SVM", classif_rate))

# get the separating hyperplane
w = clf.coef_[0]
a = -w[0] / w[1]
xx = np.linspace(-5, 5)
yy = a * xx - (clf.intercept_[0]) / w[1]


# plot the parallels to the separating hyperplane that pass through the
# support vectors
b = clf.support_vectors_[0]
yy_down = a * xx + (b[1] - a * b[0])
b = clf.support_vectors_[-1]
yy_up = a * xx + (b[1] - a * b[0])

# plot the line, the points, and the nearest vectors to the plane
pl.plot(xx, yy, 'k-')
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)

pl.axis('tight')
pl.show()

# <codecell>

b

# <codecell>

n_features = X.shape[1]

C = 1.0

# Create different classifiers. The logistic regression cannot do
# multiclass out of the box.
classifiers = {'L1 logistic': LogisticRegression(C=C, penalty='l1'),
               'L2 logistic': LogisticRegression(C=C, penalty='l2'),
               'Linear SVC': SVC(kernel='linear', C=C, probability=True)}

n_classifiers = len(classifiers)

pl.figure(figsize=(3 * 2, n_classifiers * 2))
pl.subplots_adjust(bottom=.2, top=.95)

for index, (name, classifier) in enumerate(classifiers.iteritems()):
    classifier.fit(X_scaled, y)

    y_pred = classifier.predict(X)
    classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
    print("classif_rate for %s : %f " % (name, classif_rate))

    # View probabilities=
    xx = np.linspace(3, 9, 100)
    yy = np.linspace(1, 5, 100).T
    xx, yy = np.meshgrid(xx, yy)
    Xfull = np.c_[xx.ravel(), yy.ravel()]
    probas = classifier.predict_proba(Xfull)
    n_classes = np.unique(y_pred).size
    for k in range(n_classes):
        pl.subplot(n_classifiers, n_classes, index * n_classes + k + 1)
        pl.title("Class %d" % k)
        if k == 0:
            pl.ylabel(name)
        imshow_handle = pl.imshow(probas[:, k].reshape((100, 100)),
                                  extent=(3, 9, 1, 5), origin='lower')
        pl.xticks(())
        pl.yticks(())
        idx = (y_pred == k)
        if idx.any():
            pl.scatter(X[idx, 0], X[idx, 1], marker='o', c='k')


# <codecell>

ax = pl.axes([0.15, 0.04, 0.7, 0.05])
pl.title("Probability")
pl.colorbar(imshow_handle, cax=ax, orientation='horizontal')

pl.show()

# <codecell>

dlls_of_interest = '''comdlg32.dll
gdi32.dll
gdiplus.dll
msvbvm50.dll
msvbvm60.dll
ole32.dll
setupapi.dll
user32.dll'''.split()

features = [ 'hash_id', 'pe_size', 'num_imports', 'num_sections'] + dlls_of_interest 

gui_vectors = [i.get_dll_vector(dlls_of_interest) for i in gui_values.values()]
nongui_vectors = [ i.get_dll_vector(dlls_of_interest) for i in nongui_values.values()]

target_vector = ["gui" for i in xrange(0, len(gui_vectors))] +\
                ["non_gui" for i in xrange(0, len(nongui_vectors))]

data_vectors = gui_vectors + nongui_vectors
# convert scikit Bunch

n_features = len(dlls_of_interest) # + len(['num_imports', 'num_sections']) 
n_samples = len(data)

target_names = ['gui', 'non_gui']
target = np.empty((n_samples,), dtype=np.int)
data = np.empty((n_samples, n_features))

# ignore hash_id and file size
for pos, values in enumerate(data_vectors):
    data[pos] = np.asarray(values[4:], dtype=np.float)
    target[pos] = np.asarray((1 if target_vector[pos] == 'gui' else 0), dtype=np.int)

gui_bunch = Bunch(data=data, target=target,
             target_names=target_names,
             feature_names=features[4:])

# <codecell>

# learn using sklearn's svm library
from sklearn import svm
X = data.copy()
Y = target.copy()

y = Y
clf = svm.SVC(kernel='linear')
clf.fit(X_scaled, Y)
y_pred = clf.predict(X)
classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
print("classif_rate for %s : %f " % ("SVM", classif_rate))

# <codecell>

y = Y
clf = svm.SVC(kernel='linear')
clf.fit(X_scaled, Y)

# get the separating hyperplane
w = clf.coef_[0]
a = -w[0] / w[1]
xx = np.linspace(-5, 5)
yy = a * xx - (clf.intercept_[0]) / w[1]


# plot the parallels to the separating hyperplane that pass through the
# support vectors
b = clf.support_vectors_[0]
yy_down = a * xx + (b[1] - a * b[0])
b = clf.support_vectors_[-1]
yy_up = a * xx + (b[1] - a * b[0])

# plot the line, the points, and the nearest vectors to the plane
pl.plot(xx, yy, 'k-')
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)

pl.axis('tight')
pl.show()

# <codecell>

n_features = X.shape[1]

C = 1.0

# Create different classifiers. The logistic regression cannot do
# multiclass out of the box.
classifiers = {'L1 logistic': LogisticRegression(C=C, penalty='l1'),
               'L2 logistic': LogisticRegression(C=C, penalty='l2'),
               'Linear SVC': SVC(kernel='linear', C=C, probability=True)}

n_classifiers = len(classifiers)

pl.figure(figsize=(3 * 2, n_classifiers * 2))
pl.subplots_adjust(bottom=.2, top=.95)

for index, (name, classifier) in enumerate(classifiers.iteritems()):
    classifier.fit(X_scaled, y)

    y_pred = classifier.predict(X)
    classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
    print("classif_rate for %s : %f " % (name, classif_rate))

    # View probabilities=
    xx = np.linspace(3, 9, 100)
    yy = np.linspace(1, 5, 100).T
    xx, yy = np.meshgrid(xx, yy)
    Xfull = np.c_[xx.ravel(), yy.ravel()]
    probas = classifier.predict_proba(Xfull)
    n_classes = np.unique(y_pred).size
    for k in range(n_classes):
        pl.subplot(n_classifiers, n_classes, index * n_classes + k + 1)
        pl.title("Class %d" % k)
        if k == 0:
            pl.ylabel(name)
        imshow_handle = pl.imshow(probas[:, k].reshape((100, 100)),
                                  extent=(3, 9, 1, 5), origin='lower')
        pl.xticks(())
        pl.yticks(())
        idx = (y_pred == k)
        if idx.any():
            pl.scatter(X[idx, 0], X[idx, 1], marker='o', c='k')

# <codecell>

ax = pl.axes([0.15, 0.04, 0.7, 0.05])
pl.title("Probability")
pl.colorbar(imshow_handle, cax=ax, orientation='horizontal')

pl.show()

# <codecell>

X

# <codecell>

data

# <codecell>

dlls_of_interest = '''gdi32.dll
gdiplus.dll'''.split()

features = [ 'hash_id', 'pe_size', 'num_imports', 'num_sections'] + dlls_of_interest 

gui_vectors = [i.get_dll_vector(dlls_of_interest) for i in gui_values.values()]
nongui_vectors = [ i.get_dll_vector(dlls_of_interest) for i in nongui_values.values()]

target_vector = ["gui" for i in xrange(0, len(gui_vectors))] +\
                ["non_gui" for i in xrange(0, len(nongui_vectors))]

data_vectors = gui_vectors + nongui_vectors
# convert scikit Bunch

n_features = len(dlls_of_interest) # + len(['num_imports', 'num_sections']) 
n_samples = len(data_vectors)

target_names = ['gui', 'non_gui']
target = np.empty((n_samples,), dtype=np.int)
data = np.empty((n_samples, n_features))

# ignore hash_id and file size
for pos, values in enumerate(data_vectors):
    data[pos] = np.asarray(values[4:], dtype=np.float)
    target[pos] = np.asarray((1 if target_vector[pos] == 'gui' else 0), dtype=np.int)

gui_bunch = Bunch(data=data, target=target,
             target_names=target_names,
             feature_names=features[4:])

# <codecell>

# learn using sklearn's svm library
from sklearn import svm
X = data.copy()
Y = target.copy()
X_scaled = scale(X)

y = Y
clf = svm.SVC(kernel='linear')
clf.fit(X_scaled, Y)
y_pred = clf.predict(X)
classif_rate = np.mean(y_pred.ravel() == y.ravel()) * 100
print("classif_rate for %s : %f " % ("SVM", classif_rate))

# <codecell>

y

# <codecell>

X_scaled

# <codecell>

np.max(X, axis = 1)

# <codecell>

X

# <codecell>

data

# <codecell>


