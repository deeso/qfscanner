import logging
import traceback

from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import sessionmaker 
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine, ForeignKey, pool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import select, and_
from sqlalchemy import MetaData, Column, Date, BigInteger, Integer, Text, Table, Boolean, REAL

loggers = {}
def get_my_logger(name, lvl):
    global loggers
    if name in loggers:
        log = loggers[name]
        log.setLevel(lvl)
        return log 

    formatter = logging.Formatter( \
            '[%(levelname)s] [%(asctime)s] *%(name)s* %(message)s', \
            "%Y-%m-%d %H:%M:%S")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    log = logging.getLogger(name)
    log.setLevel(lvl)
    log.addHandler(handler)
    loggers[name] = log
    return log

Base = declarative_base()

class ImportAssociations(Base):
    __tablename__ = "import_associations"
    hash_id = Column(BigInteger, unique=True, 
                        primary_key=True, 
                        autoincrement=False)
    
    Pefile_hash_id = Column(BigInteger, 
                        ForeignKey('pefiles.hash_id', 
                            name='fk_import_associations_Pefile'))
    ImportRelations_hash_id = Column(BigInteger, 
                        ForeignKey('imports.hash_id', 
                            name='fk_import_associations_ImportRelations'))

    #extra_data = Column(Text(50))
    #child = relationship("Child")

    
    Risk = Column(REAL)
    DelayLoaded = Column(Boolean)
    Forwarded = Column(Boolean)

    IsSuspicious = Column(Boolean)
    Reason  = Column(Text)


class ExportAssociations(Base):
    __tablename__ = "export_associations"

    hash_id = Column(BigInteger, unique=True, 
                        primary_key=True, 
                        autoincrement=False)

    Pefile_hash_id = Column(BigInteger, 
                        ForeignKey('pefiles.hash_id', 
                            name='fk_export_associations_Pefile'))

    ExportRelations_hash_id = Column(BigInteger, 
                        ForeignKey('exports.hash_id', 
                            name='fk_export_associations_ExportRelations'))
    

    Risk = Column(REAL)
    DelayLoaded = Column(Boolean)
    Forwarded = Column(Boolean)

    IsSuspicious = Column(Boolean)
    Reason  = Column(Text)


class PEFile(Base):
    __tablename__ = "pefiles"

    hash_id = Column(BigInteger, unique=True, primary_key=True, autoincrement=False)
    export_association = relationship("ExportAssociations", backref="PEFile")


    import_association = relationship("ImportAssociations", backref="PEFile")
    
    DelayLoadedCnt = Column(BigInteger)
    ForwardedCnt = Column(BigInteger)
    NumberOfImportFunctions = Column(BigInteger)
    NumberOfImportFiles = Column(BigInteger)
    NumberOfExports = Column(BigInteger)
    
    FileName = Column(Text)
    FilePath = Column(Text)
    Extension = Column(Text)
    FileSize = Column(BigInteger)
    isDll = Column(Boolean)
    bits = Column(Integer)
    isSuspicious = Column(Text)
    Reason = Column(Text)
    Md5 = Column(Text)
    Sha256 = Column(Text)
    Entropy = Column(REAL)
    TimeDateStamp = Column(BigInteger)
    NumberOfSections = Column(BigInteger)
    SizeOfImage = Column(BigInteger)
    SizeOfOptionalHeader = Column(BigInteger)
    SizeOfCode = Column(BigInteger)
    SizeOfInitializedData = Column(BigInteger)
    SizeOfUninitializedData = Column(BigInteger)
    SizeOfHeaders = Column(BigInteger)
    LoaderFlags = Column(BigInteger)
    NumberOfRvaAndSizes = Column(BigInteger)
    NumberOfSymbols = Column(BigInteger)
    SizeOfStackReserve = Column(BigInteger)
    SizeOfStackCommit = Column(BigInteger)
    SizeOfHeapReserve = Column(BigInteger)
    SizeOfHeapCommit = Column(BigInteger)
    CompletedProcessing = Column(BigInteger)

class Export(Base):
    __tablename__ = "exports"
    hash_id = Column(BigInteger, unique=True, primary_key=True, autoincrement=False)
    SymbolName = Column(Text)
    DllName = Column(Text)
    ExportRelations = relationship("ExportAssociations",
                backref=backref("Export",
                                cascade="all"))
    ParsingError = Column(Boolean)
    Hexlified = Column(Text)


class Import(Base):
    __tablename__ = "imports"
    hash_id = Column(BigInteger, unique=True, primary_key=True, autoincrement=False)
    SymbolName = Column(Text)
    DllName = Column(Text)
    ImportRelations = relationship("ImportAssociations",
                backref=backref("Import",
                                cascade="all"))
    ParsingError = Column(Boolean)
    Hexlified = Column(Text)

class ExportRelations(Base):
    __tablename__ = 'ExportRelations'

    exports_hash_id = Column(BigInteger, 
                        ForeignKey('exports.hash_id'),
                        primary_key=True)

    export_associations_hash_id = Column(BigInteger, 
                        ForeignKey('export_associations.hash_id'),
                        primary_key=True)
    

class ImportRelations(Base):
    __tablename__ = 'ImportRelations'

    imports_hash_id = Column(BigInteger, 
                        ForeignKey('imports.hash_id', 
                            name='fk_ImportRelations_key1'),
                        primary_key=True)

    import_associations_hash_id = Column(BigInteger, 
                        ForeignKey('import_associations.hash_id', ondelete='SET NULL',
                            name='fk_ImportRelations_key2'),
                        primary_key=True)



class FileSummaries(Base):
    __tablename__ = "file_summaries"
    id = Column(BigInteger, unique=True, primary_key=True, autoincrement=False)
    FileName = Column(Text)
    FilePath = Column(Text)
    Extension = Column(Text)
    FailedProcessing = Column(Boolean)
    Md5 = Column(Text)
    Sha256 = Column(Text)


class Section(Base):
    __tablename__ = 'sections'
    
    id = Column(BigInteger, unique=True, primary_key=True, autoincrement=False)
    Pefile_hash_id = Column(BigInteger, 
                        ForeignKey('pefiles.hash_id', 
                            name='fk_sections_Pefile'))
    hash_id = Column(BigInteger, autoincrement=True)
    PEName = Column(Text)
    SectionName = Column(Text)
    VirtualAddr = Column(BigInteger)
    VirtualSize = Column(BigInteger)
    RawSize = Column(BigInteger)
    Entropy = Column(REAL)
    Characteristics = Column(BigInteger)
    ParsingError = Column(Boolean)
    Hexlified = Column(Text)


class PostGresFE(object):
    def __init__(self, user, password, hostname="localhost", dbname='pescan', 
            create_db=False, create_all=False, debug_lvl=None, **kargs):
        
        debug_lvl = logging.INFO if debug_lvl is None else debug_lvl
        
        uri_args = {'user':user, 'password':password, 'hostname':hostname}
        self.uri = "postgres://{user}:{password}@{hostname}".format(**uri_args)

        uri_args['dbname'] = dbname
        self.db_uri = "postgres://{user}:{password}@{hostname}/{dbname}".format(**uri_args)
        
        self.engine = create_engine(self.db_uri, **kargs)
        self.logger = get_my_logger("PostGresFEDefault", debug_lvl)
        
        if create_db:
            self.engine = create_engine(self.uri, **kargs)
            self.create_db(dbname)
            self.engine = create_engine(self.db_uri, **kargs) 
            create_all = True

        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
        
        if create_all:
            self.drop_all()
            Base.metadata.create_all(self.engine)

    def create_db(self, dbname):
        conn = self.engine.connect()
        conn.execute('commit')
        try:
            conn.execute("create database %s"%dbname)
        except:
            self.logger.warn(traceback.format_exc())
        conn.close()
         
    def drop_all(self):
        meta = MetaData(self.engine)
        meta.reflect()
        try:
            meta.drop_all()
        except:
            self.logger.warn(traceback.format_exc())



class SQLiteFE(object):
    def __init__(self, uri='sqlite:///:memory:', dbname="default.db",
        create_db=False, create_all=False, debug_lvl=None, **kargs):
        
        debug_lvl = logging.INFO if debug_lvl is None else debug_lvl

        self.connection = apsw.Connection(':memory:')
        self.dbname = dbname
        self.pool = pool.SingletonThreadPool (lambda: sqlite3.connect(self.connection))
        self.uri = uri
        
        Base.metadata.create_all(self.engine)
        self.setup_backend(uri, pool=self.pool, **kargs)
            
        if create_db:
            self.create_db()
            create_all = True

        self.session = self.Session()
        
        if create_all:
            self.drop_all()
            Base.metadata.create_all(self.engine)

    def create_db(self):
        conn = self.engine.connect()
        self.backup_sqlite()
        conn.close()

         
    def drop_all(self):
        meta = MetaData(self.engine)
        meta.reflect()
        try:
            meta.drop_all()
        except:
            self.logger.warn(traceback.format_exc())

    def setup_backend(self, init_string, **kargs):
        self.engine = create_engine(init_string, **kargs)
        self.Session = sessionmaker(bind=engine) 
        
    def backup_sqlite(self):
        new_conn = apsw.Connection(self.dbname)
        with new_conn.backup("main", self.connection, "main") as backup:
            backup.step() # copy whole database in one go
        new_conn.close()

